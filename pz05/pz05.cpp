//
// Created by user on 10/19/21.
//

#include "../catch.hpp"
#include "../pz03/task3.h"
#include <fstream>

using namespace custom_space;

int spell_check(std::string target, std::string perfect) {
    std::ifstream input_file;
    input_file.open(target, std::ios_base::binary);
    std::ifstream perfect_file;
    perfect_file.open(perfect, std::ios_base::binary);
    char first, second;
    while (!input_file.eof()) {
        input_file >> first;
        perfect_file >> second;
        if (first != second) {
            input_file.close();
            perfect_file.close();
            return -1;
        }
    }
    input_file.close();
    perfect_file.close();
    return 1;
}

TEST_CASE("custom_copy_correct", "[correct_data]") {
    REQUIRE(custom_copy("../pz05/data/1_correct_input.txt", \
    "../pz05/data/1_output.txt") == 1);
}

TEST_CASE("float_addition_from_file_correct", "[correct_data]") {
    REQUIRE(float_addition_from_file("../pz05/data/2_correct_input.txt") == 2.5);
}

TEST_CASE("custom_encryption_correct", "[correct_data]") {
    REQUIRE(custom_encryption("../pz05/data/3_correct_input.txt", "../pz05/data/3_output.txt", "herr_poluvasyan") == 12);
    custom_encryption("../pz05/data/3_output.txt", "../pz05/data/3_check_output.txt", "herr_poluvasyan");
    REQUIRE(spell_check("../pz05/data/3_check_output.txt", "../pz05/data/perfect.txt") == 1);
}


TEST_CASE("custom_copy_incorrect", "incorrect_data[]") {
    REQUIRE(custom_copy("../pz05/data/1_correct_infsdfsdfut.txt", \
    "../pz05/data/1_output.txt") == input_file_not_opening );
}

TEST_CASE("float_addition_from_file_incorrect", "incorrect_data[]") {
    REQUIRE(float_addition_from_file("../pz05/data/2_incorrect_input.txt") == first_summand_is_negative);
}

TEST_CASE("custom_encryption_incorrect", "incorrect_data[]") {
    REQUIRE(custom_encryption("../pz05/data/3_incorrect_input.tt", "../pz05/data/3_incorrect_output.txt", "herr_poluvasyan") == input_file_not_opening );
}


TEST_CASE("custom_copy_absent", "[absent_data]") {
    REQUIRE(custom_copy("", "") == input_string_is_empty);
}
TEST_CASE("float_addition_from_file_absent", "[absent_data]") {
    REQUIRE(float_addition_from_file("../pz05/data/2_absent_input.txt") == input_file_no_number);
}
TEST_CASE("custom_encryption_absent", "[absent_data]") {
    REQUIRE(custom_encryption("../pz05/data/3_absent_input.tt", "../pz05/data/3_absent_output.txt", "") == encryption_key_empty);
}