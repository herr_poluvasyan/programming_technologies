#include "behaviour_server.h"


std::vector<Poco::Net::StreamSocket> connected_sockets;

void SendMessage(Poco::Net::StreamSocket &ss, std::string message) {
    nlohmann::json json;
    json["from"] = "host";
    json["message"] = message;
    std::string request = json.dump();
    ss.sendBytes(request.data(), request.length() + 1);
}


void ServerMessaging(Poco::Net::StreamSocket &ss) {
    bool isOpen = true;
    Poco::Timespan timeOut(10, 0);
    char incommingBuffer[1000];

    while (isOpen) {
        if (ss.poll(timeOut, Poco::Net::Socket::SELECT_READ) == false) {
            std::cout << "TIMEOUT!" << std::endl << std::flush;
        } else {
            std::cout << "RX EVENT!!! ---> " << std::flush;
            int nBytes = -1;

            try {
                nBytes = ss.receiveBytes(incommingBuffer, sizeof(incommingBuffer));
            }
            catch (Poco::Exception &exc) {
                //Handle your network errors.
                std::cerr << "Network error: " << exc.displayText() << std::endl;
                isOpen = false;
            }

            if (nBytes == 0) {
                std::cout << "Client closes connection!" << std::endl << std::flush;
                isOpen = false;
            } else {
                std::string command(incommingBuffer);
            }
        }
    }
    std::cout << "Connection finished!" << std::endl << std::flush;
}


void behaviour_server::run() {

    std::cout << "New connection from: " << socket().peerAddress().host().toString() << std::endl << std::flush;

    behaviour_server::amount_of_connections++;

    char incommingBuffer[1000];
    bool isOpen = true;
    bool documented(false);
    Poco::Timespan timeOut(100, 0);

    while (isOpen) {
        std::fstream current_connections;
        current_connections.open("../pz07/connections.json", std::fstream::in | std::fstream::out | std::fstream::app);
        if (!current_connections.is_open()) {
            std::cout << "Can't reach connections.json" << std::endl;
        }
        std::fstream user_messages;
        user_messages.open("../pz07/log.json", std::fstream::in | std::fstream::out | std::fstream::app);
        if (!user_messages.is_open()) {
            std::cout << "Can't reach log.json" << std::endl;
        }
        std::fstream multi_messages;
        multi_messages.open("../pz07/AllLog.json", std::fstream::in | std::fstream::out | std::fstream::app);
        if (!multi_messages.is_open()) {
            std::cout << "Can't reach AllLog.json" << std::endl;
        }
        if (socket().poll(timeOut, Poco::Net::Socket::SELECT_READ) == false) {
            std::cout << "TIMEOUT!" << std::endl << std::flush;
        } else {
            std::cout << "RX EVENT!!! ---> " << std::flush;
            int nBytes = -1;

            try {
                nBytes = socket().receiveBytes(incommingBuffer, sizeof(incommingBuffer));
            }
            catch (Poco::Exception &exc) {
                std::cerr << "Network error: " << exc.displayText() << std::endl;
                isOpen = false;
            }
            if (nBytes == 0) {
                std::cout << "Client closes connection!" << std::endl << std::flush;
                isOpen = false;
            } else {

                nlohmann::json message = nlohmann::json::parse(incommingBuffer);

                if (documented == false) {
                    current_connections << "{\"ip\":" << socket().peerAddress().host()                               \
 << ",\"port\":" << socket().peerAddress().port()                                                                    \
 << ",\"username\":" << message["from"] << "}" << std::endl;
                    documented = true;
                    connected_sockets.push_back(socket());
                }

                if (message["multi"] == 1) {
                    for (int i = 0; i < connected_sockets.size(); i++) {
                        SendMessage(connected_sockets[i], message["message"]);
                    }
                    std::cout << "Client send message: " << message["message"] << std::endl << std::flush;
                    multi_messages << "{" << std::endl << "\"client\":" << message["from"] << "," << std::endl            \
 << "\"ip\":\"" << socket().peerAddress().host() << "," << std::endl                                                 \
 << "\"port\":" << socket().peerAddress().port() << "," << std::endl                                                 \
 << "\"message\":" << message["message"] << std::endl << "}" << std::endl;
                } else {
                    std::cout << "Client send message: " << message["message"] << std::endl << std::flush;
                    SendMessage(socket(), message["message"].get<std::string>());

                    user_messages << "{" << std::endl << "\"client\":" << message["from"] << "," << std::endl            \
 << "\"ip\":\"" << socket().peerAddress().host() << "," << std::endl                                                 \
 << "\"port\":" << socket().peerAddress().port() << "," << std::endl                                                 \
 << "\"message\":" << message["message"] << std::endl << "}" << std::endl;
                }
            }
        }
        current_connections.close();
        user_messages.close();
        multi_messages.close();
    }
    std::cout << "Connection finished!" << std::endl << std::flush;
}

