#include "behaviour_server.h"


int main(int argc, char **argv) {

    std::fstream current_connections;
    current_connections.open("../pz07/connections.json", std::fstream::out | std::fstream::trunc);
    current_connections.close();
    std::fstream user_messages;
    user_messages.open("../pz07/log.json", std::fstream::out | std::fstream::trunc);
    user_messages.close();
    std::fstream multi_messages;
    multi_messages.open("../pz07/AllLog.json", std::fstream::out | std::fstream::trunc);
    multi_messages.close();


    Poco::Net::ServerSocket svs(1234);

    Poco::Net::TCPServerParams *pParams = new Poco::Net::TCPServerParams();
    pParams->setMaxThreads(4);
    pParams->setMaxQueued(4);
    pParams->setThreadIdleTime(100);


    Poco::Net::TCPServer myServer(new Poco::Net::TCPServerConnectionFactoryImpl<behaviour_server>(), svs, pParams);
    myServer.start();


    while (true) {
        int choice(-1);
        std::cout << "To exit enter 0" << std::endl                       \
 << "To show log enter 1" << std::endl                                    \
 << "To show all active connections enter 2" << std::endl                 \
 << "To show Alllog enter 3" << std::endl;
        std::cin >> choice;
        switch (choice) {
            case 0: {
                return 0;
            }
            case 1: {
                std::fstream user_messages;
                user_messages.open("../pz07/log.json", std::fstream::in);
                std::cin.ignore();
                while(!user_messages.eof()) {
                    std::string temporary;
                    std::getline(user_messages, temporary);
                    std::cout << "\t" << temporary << std::endl;
                }
                user_messages.close();
                break;
            }
            case 2: {
                std::fstream current_connections;
                current_connections.open("../pz07/connections.json", std::fstream::in);
                std::cin.ignore();
                while(!current_connections.eof()) {
                    std::string temporary;
                    std::getline(current_connections, temporary);
                    std::cout << "\t" << temporary << std::endl;
                }
                current_connections.close();
                break;
            }
            case 3: {
                std::fstream multi_messages;
                multi_messages.open("../pz07/AllLog.json", std::fstream::in);
                std::cin.ignore();
                while(!multi_messages.eof()) {
                    std::string temporary;
                    std::getline(multi_messages, temporary);
                    std::cout << "\t" << temporary << std::endl;
                }
                multi_messages.close();
                break;
            }
            default: {
                std::cout << "Wrong input, try again" << std::endl;
                break;
            }
        }
    }
    return 0;
}
