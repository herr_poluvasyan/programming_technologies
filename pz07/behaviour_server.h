#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/Socket.h"
#include <nlohmann/json.hpp>
#include <thread>

#ifndef CLEARTCPCLIENT_BEHAVIOUR_SERVER_H
#define CLEARTCPCLIENT_BEHAVIOUR_SERVER_H



class behaviour_server : public Poco::Net::TCPServerConnection {
public:
    std::vector<std::string> connection_list;

    int amount_of_connections = 0;

    behaviour_server(const Poco::Net::StreamSocket &s) :
            Poco::Net::TCPServerConnection(s) {

    }

    void run();
};

#endif //CLEARTCPCLIENT_BEHAVIOUR_SERVER_H
