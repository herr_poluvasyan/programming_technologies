#include <iostream>
#include <fstream>
#include <string>
#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/Socket.h"
#include <nlohmann/json.hpp>
#include <thread>

void ServerMessaging(Poco::Net::StreamSocket &ss) {
    std::fstream user_01_messages;
    user_01_messages.open("../pz07/01_messages.json", std::fstream::in | std::fstream::out | std::fstream::app);
    if (!user_01_messages.is_open()) {
        std::cout << "Can't reach log.json" << std::endl;
    }
    bool isOpen = true;
    Poco::Timespan timeOut(100, 0);
    char incommingBuffer[1000];
    while (isOpen) {
        if (ss.poll(timeOut, Poco::Net::Socket::SELECT_READ) == false) {
            std::cout << "TIMEOUT!" << std::endl << std::flush;
        } else {
            std::cout << "RX EVENT!!! ---> " << std::flush;
            int nBytes = -1;

            try {
                nBytes = ss.receiveBytes(incommingBuffer, sizeof(incommingBuffer));
            }
            catch (Poco::Exception &exc) {
                //Handle your network errors.
                std::cerr << "Network error: " << exc.displayText() << std::endl;
                isOpen = false;
            }

            if (nBytes == 0) {
                std::cout << "Client closes connection!" << std::endl << std::flush;
                isOpen = false;
            } else {
                nlohmann::json message = nlohmann::json::parse(incommingBuffer);
                std::cout << "Client send message: " << message["message"] << std::endl << std::flush;

                user_01_messages << "{" << std::endl << "\"client\":" << message["from"] << "," << std::endl            \
 << "\"ip\":\"" << ss.peerAddress().host() << "," << std::endl                                                 \
 << "\"port\":" << ss.peerAddress().port() << "," << std::endl                                                 \
 << "\"message\":" << message["message"] << std::endl << "}" << std::endl;
            }
        }
    }
    std::cout << "Connection finished!" << std::endl << std::flush;
}

void SendMessage(Poco::Net::StreamSocket &ss, std::string message, int multiple) {
    nlohmann::json json;
    json["from"] = "herr_poluvasyan";
    json["message"] = message;
    json["multi"] = multiple;
    std::string request = json.dump();
    ss.sendBytes(request.data(), request.length() + 1);
}


int main() {

    std::fstream user_01_messages;
    user_01_messages.open("../pz07/01_messages.json", std::fstream::out | std::fstream::trunc);
    user_01_messages.close();

    Poco::Net::StreamSocket ss;
    std::string message;
    ss.connect(Poco::Net::SocketAddress("localhost", 1234));
    std::thread startMessaging(ServerMessaging, std::ref(ss));
    message = getlogin();
    SendMessage(ss, message, 0);

    while (true) {
        int choice(-1);
        std::cout << "To exit enter 0" << std::endl                          \
 << "To send message to the server enter 1" << std::endl                     \
 << "To send message to all active users enter 2" << std::endl               \
 << "To show log enter 3" << std::endl;
        std::cin >> choice;
        switch (choice) {
            case 0: {
                return 0;
                break;
            }
            case 1: {
                std::cin.ignore();
                std::getline(std::cin, message);
                SendMessage(ss, message, 0);
                break;
            }
            case 2: {
                std::cin.ignore();
                std::getline(std::cin, message);
                SendMessage(ss, message, 1);
                break;
            }
            case 3: {
                std::fstream messages;
                messages.open("../pz07/01_messages.json", std::fstream::in);
                std::cin.ignore();
                while (!messages.eof()) {
                    std::string temporary;
                    std::getline(messages, temporary);
                    std::cout << "\t" << temporary << std::endl;
                }
                messages.close();
                break;
            }
            default: {
                std::cout << "Wrong input, try again" << std::endl;
                break;
            }
        }

    }

    return 0;
}
