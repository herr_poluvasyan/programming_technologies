//
// Created by user on 10/21/21.
//

#include <SFML/Graphics.hpp>
#include <string>
#include <thread>

void PollEventFunc(sf::RenderWindow &window) {
    sf::RenderWindow new_window(sf::VideoMode(200, 200), "2020-3-22-fol");
    new_window.setActive();
    sf::RectangleShape rectagle;
    rectagle.setPosition(50,50); //устанавливаем кординаты фигуры (x,y)
    rectagle.setFillColor(sf::Color::Red); //задаём цвет внутри фигуры - красный
    rectagle.setOutlineColor(sf::Color::Black); //задаём цвет рамки вокруг фигуры
    rectagle.setOutlineThickness(1);// задаём ширину рамки вокруг фигуры
    sf::Vector2f vector(20,20);// создаём двумерный вектор, который будет описывать размеры фигуры: ширина и длинна
    rectagle.setSize(vector);

    while (new_window.isOpen()) {
        sf::Event event;
        while (new_window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                new_window.close();
        }
        new_window.clear();
        new_window.draw(rectagle);
        new_window.display();
    }
    new_window.setActive(false);
}


int main() {
    std::string keyEnteredMessage;

    sf::RenderWindow window(sf::VideoMode(200, 200), "2020-3-22-fol");

    sf::Font font;

    if (!font.loadFromFile("/usr/share/fonts/truetype/ubuntu/Ubuntu-BI.ttf")) {
        return 0;
    }

    sf::Text textKey("", font, 20);
    textKey.setColor(sf::Color::Red);
    textKey.setPosition(0, 50);

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed: {
                    window.close();
                    break;
                }
                case sf::Event::TextEntered: {
                    switch (event.text.unicode) {
                        case 8: {
                            keyEnteredMessage.pop_back();
                            break;
                        }
                        case 127: {
                            keyEnteredMessage.pop_back();
                            break;
                        }
                        default: {
                            keyEnteredMessage += (char) event.text.unicode;
                            break;
                        }
                    }
                    textKey.setString(keyEnteredMessage);
                }
                case sf::Event::KeyPressed: {
                    if ((event.key.code == sf::Keyboard::Return) && (keyEnteredMessage == "window")) {
                        window.setActive(false);
                        sf::Thread new_window_thread(PollEventFunc, std::ref(window));
                        new_window_thread.launch();
                        new_window_thread.wait();
                        window.setActive(true);
                    }
                    break;
                }
            }
        }
        window.clear();
        window.draw(textKey);
        window.display();
        //test
    }

    return 0;
}

