//
// Created by user on 10/4/21.
//

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <algorithm>

namespace custom_space {
    /*
     * -5 represents failed attempt to open input file
     * -6 represents failed attempt to open or create output file
     */
    const int input_file_not_opening        = -5;
    const int output_file_not_opening       = -6;
    const int input_file_no_number          = -7;
    const int input_file_one_number         = -8;
    const int input_string_is_empty         = -9;
    const int first_summand_is_negative     = -10;
    const int second_summand_is_negative    = -11;
    const int first_summand_wrong_format    = -12;
    const int second_summand_wrong_format   = -13;
    const int encryption_key_empty          = -14;

    /*
     * custom_copy takes input file name and output file name
     * copies one file in to another line by line
     * returns one of the error codes depicted above or returns number of
     * copied lines when finished
     */
    int custom_copy(std::string, std::string);

    /*
     * float_addition_from_file takes two strings from file
     * changes , into . if necessary then converts strings to double
     * and returns the sum of them
     */
    double float_addition_from_file(std::string);

    /*
     * custom_encryption takes input file name, output file name and an encryption key
     * function takes one byte from input file, than encrypt it using one of the bytes of
     * the encryption key and saves it to the output file byte by byte
     * returns the number of encrypted bytes
     */
    int custom_encryption(std::string, std::string, std::string);
}