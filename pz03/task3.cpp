//
// Created by user on 9/27/21.
//

#include "task3.h"

namespace custom_space {
    int custom_copy(std::string target, std::string result) {
        if (target.empty() || result.empty()) {
            std::cout << "Error: empty input" << std::endl;
            return input_string_is_empty;
        }
        int copied_lines(0);
        std::ifstream input_file;
        input_file.open(target, std::ios_base::in);
        std::ofstream output_file;
        output_file.open(result, std::ios_base::app | std::ios_base::out);
        if (!input_file) {
            std::cout << "Error: target file can not be opened." << std::endl;
            return input_file_not_opening;
        }
        if (!output_file) {
            std::cout << "Error: output file can not be neither opened nor created." << std::endl;
            return output_file_not_opening;
        }
        while (!input_file.eof()) {
            ++copied_lines;
            std::string temporary_buffer_line;
            std::getline(input_file, temporary_buffer_line);
            output_file << temporary_buffer_line << std::endl;
        }
        input_file.close();
        output_file.close();
        std::cout << std::endl;
        std::cout << "Copied " << copied_lines << " lines successfully." << std::endl;
        return copied_lines;
    }

    double float_addition_from_file(std::string target) {
        if (target.empty()) {
            std::cout << "Error: empty input" << std::endl;
            return input_string_is_empty;
        }
        std::ifstream input_file;
        input_file.open(target, std::ios_base::in);
        if (!input_file) {
            std::cout << "Error: target file can not be opened." << std::endl;
            return input_file_not_opening;
        }
        std::string first_summand;
        input_file >> first_summand;
        if (first_summand[0] == '\0') {
            std::cout << "Error: input file contains no numbers" << std::endl;
            return input_file_no_number;
        }
        std::string second_summand;
        input_file >> second_summand;
        if (second_summand[0] == '\0') {
            std::cout << "Error: input file contains only one number" << std::endl;
            return input_file_one_number;
        }
        if (!first_summand.find('-')) {
            std::cout << "Error: first summand is negative" << std::endl;
            return first_summand_is_negative;
        }
        if (!second_summand.find('-')) {
            std::cout << "Error: second summand is negative" << std::endl;
            return second_summand_is_negative;
        }
        if (std::count(first_summand.begin(), first_summand.end(), '.') + std::count(first_summand.begin(), first_summand.end(), ',') > 1) {
            std::cout << "Error, first summand - wrong format" << std::endl;
            return first_summand_wrong_format;
        }
        if (std::count(second_summand.begin(), second_summand.end(), '.') + std::count(second_summand.begin(), second_summand.end(), ',') > 1) {
            std::cout << "Error, second summand - wrong format" << std::endl;
            return second_summand_wrong_format;
        }
        unsigned long separator_position = first_summand.find(',');
        first_summand[separator_position] = '.';
        double first_summand_converted = std::atof(first_summand.c_str());
        separator_position = second_summand.find(',');
        second_summand[separator_position] = '.';
        double second_summand_converted = std::atof(second_summand.c_str());
        input_file.close();
        return first_summand_converted + second_summand_converted;
    }

    int custom_encryption(std::string target, std::string result, std::string encryption_key) {
        if (target.empty() || result.empty()) {
            std::cout << "Error: empty input" << std::endl;
            return input_string_is_empty;
        }
        if (encryption_key.empty()) {
            std::cout << "Error: encryption fraze is empty" << std::endl;
            return encryption_key_empty;
        }
        std::ifstream input_file;
        input_file.open(target, std::ios_base::in);
        std::ofstream output_file;
        output_file.open(result, std::ios_base::app | std::ios_base::out);
        if (!input_file) {
            std::cout << "Error: target file can not be opened." << std::endl;
            return input_file_not_opening;
        }
        if (!output_file) {
            std::cout << "Error: output file can not be neither opened nor created." << std::endl;
            return output_file_not_opening;
        }
        char symbol;
        int temporary_counter(0);
        input_file.get(symbol);
        while (!input_file.eof() && symbol != '\0') {
            symbol = symbol ^ encryption_key[temporary_counter % encryption_key.size() + 1];
            output_file << symbol;
            ++temporary_counter;
            input_file.get(symbol);
        }
        input_file.close();
        output_file.close();
        return temporary_counter;

    }

}