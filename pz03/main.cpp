//
// Created by user on 9/27/21.
//

#include "task3.h"

using namespace custom_space;

int main() {
    setlocale(LC_CTYPE, "UTF-8");

    short int input;
    while (true) {
        std::cout << "0 shuts down the programm" << std::endl << \
        "1 starts copying function" << std::endl << \
        "2 starts summing double type sequence" \
 << std::endl << "3 starts file encoding" << std::endl;

        std::cin >> input;

        switch (input) {
            case 1: {
                std::cout << "Enter which file to copy and direction" << std::endl;
                std::string target_file, result_file;
                std::cin.ignore();
                std::getline (std::cin, target_file);
                std::getline (std::cin, result_file);
                custom_copy(target_file, result_file);
                break;
            }
            case 2: {
                std::cout << "Enter input file" << std::endl;
                std::string target_file;
                std::cin.ignore();
                std::getline(std::cin, target_file);
                double temporary = float_addition_from_file(target_file);
                if (temporary >= 0.0) {
                    std::cout << "Result: " << std::setprecision(10) << float_addition_from_file(target_file) << std::endl;
                }
                break;
            }
            case 3: {
                std::cout << "Enter input file, output file and encryption key" << std::endl;
                std::string target_file, result_file, encryption_key;
                std::cin.ignore();
                std::getline(std::cin, target_file);
                std::getline(std::cin, result_file);
                std::getline(std::cin, encryption_key);
                int temporary = custom_encryption(target_file, result_file, encryption_key);
                if (temporary >= 0) {
                    std::cout << "Successfully encrypted " << temporary << " bytes" << std::endl;
                }
                break;
            }
            case 0: {
                return 0;
            }
            default: {
                std::cout << "Wrong input" << std::endl;
                break;
            }
        }
    }
}
