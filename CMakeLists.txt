cmake_minimum_required(VERSION 3.13)
project(SQLite_TestApp)

enable_language(C)
enable_language(CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Poco REQUIRED COMPONENTS Foundation Util Net XML JSON)

if(MSVC) # WIN32
    SET(Poco_INCLUDE_DIRS "C:/Program Files/Poco/include")
else()
    SET(Poco_INCLUDE_DIRS "/usr/local/include/Poco")
endif(MSVC)

MESSAGE( [Main] " Poco_INCLUDE_DIRS = ${Poco_INCLUDE_DIRS}")
MESSAGE( [Main] " Poco_LIBRARIES = ${Poco_LIBRARIES}")

include_directories(
        ${MY_SRC_INCLUDE}
        ${Poco_INCLUDE_DIRS}
)

link_directories(${CMAKE_BINARY_DIR})

add_executable(00ClearTCPClient pz07/client00.cpp)
target_link_libraries(00ClearTCPClient ${Poco_LIBRARIES})

add_executable(01ClearTCPClient pz07/client01.cpp)
target_link_libraries(01ClearTCPClient ${Poco_LIBRARIES})

add_executable(ClearTCPServer pz07/server.cpp pz07/behaviour_server.h pz07/behaviour_server.cpp)
target_link_libraries(ClearTCPServer ${Poco_LIBRARIES})


set(CMAKE_CXX_STANDART 14)

set(CMAKE_MODULE_PATH "/usr/share/SFML/cmake/Modules/" ${CMAKE_MODULE_PATH}})

find_package(SFML 2.4.2 COMPONENTS graphics audio REQUIRED)



add_executable(pz03_main pz03/main.cpp pz03/task3.cpp pz03/task3.h)

add_executable(pz05 pz05/pz05_main.cpp pz05/pz05.cpp pz03/task3.h pz03/task3.cpp)

add_executable(pz06 pz06/main.cpp)

target_link_libraries(pz06 sfml-graphics sfml-audio sfml-window sfml-system)




add_executable(SQLite_TestApp  pz10/main.cpp)
target_link_libraries(SQLite_TestApp LINK_PUBLIC sqlite3)