#include <iostream>
#include "sqlite3.h"
#include <string>
#include <fstream>

static int callback(void *data, int argc, char **argv, char **azColName) {
    std::fstream output_file;
    output_file.open((char *) data, std::ios::in | std::ios::out | std::ios::ate);
    if (!output_file) {
        std::cout << "Error: output file can not be neither opened nor created." << std::endl;
        return -1;
    }
    for (int i = 0; i < argc; i++) {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        output_file << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << " ";
    }
    output_file << std::endl;
    output_file.close();
    printf("\n");
    return 0;
}

void
insert_function(sqlite3 *database_pointer, void *data, char *error_message, std::string username, std::string birthday,
                std::string city, long int phone_number) {
    std::string insert_sql_command = "INSERT INTO Students (username,birthday,city,phonenumber) VALUES ";
    insert_sql_command = insert_sql_command + "('" + username + "'" + ",'" + birthday + "'" + ",'" + city + "'" + "," +
                         std::to_string(phone_number) + ");";
    int result = sqlite3_exec(database_pointer, insert_sql_command.c_str(), callback, (void *) data, &error_message);
    if (result != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", error_message);
    }
}

void select_function(sqlite3 *database_pointer, void *data, char *error_message) {
    char *select_sql_command = "Select * from Students";
    int result = sqlite3_exec(database_pointer, select_sql_command, callback, (void *) data, &error_message);
    if (result != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", error_message);
        sqlite3_free(error_message);
    } else {
        //fprintf(stderr, "Inserted data successfully\n");
    }
}

void upgrade_function(sqlite3 *database_pointer, void *data, char *error_message, int id, std::string chosen_field, std::string change) {
    std::string upgrade_sql_command = "UPDATE Students SET ";
    upgrade_sql_command = upgrade_sql_command + chosen_field + " = " + "\"" + change + "\"" + " where id = " + std::to_string(id);
    int result = sqlite3_exec(database_pointer, upgrade_sql_command.c_str(), callback, (void *) data, &error_message);
    if (result != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", error_message);
        sqlite3_free(error_message);
    } else {
        //fprintf(stderr, "Inserted data successfully\n");
    }
}

//below lies code which allows to use string input in switch construction

enum string_codes {
    input = 1,
    search = 2,
    change = 3,
    sequence_exit = 0,
};

string_codes hashit(std::string const &input_string) {
    if (input_string == "1") return input;
    if (input_string == "2") return search;
    if (input_string == "3") return change;
    if (input_string == "0") return sequence_exit;
}


int main(int argc, char *argv[]) {
    sqlite3 *database_pointer;
    char *error_messages = NULL;
    int database_identificator;
    std::string sql_request;
    const char *data = "../pz10/2020-3-22-fol-Students.txt";

    std::ofstream output_file;
    output_file.open((char *) data);
    output_file.close();

    database_identificator = sqlite3_open("2020-3-22-fol.db", &database_pointer);

    if (database_identificator) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(database_pointer));
        return 0;
    } else {
        fprintf(stderr, "Opened database successfully\n");
    }
    sql_request = "CREATE TABLE IF NOT EXISTS Students (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, birthday TEXT, city TEXT, phonenumber LONG INTEGER,  UNIQUE(username));";
    database_identificator = sqlite3_exec(database_pointer,
                                          sql_request.c_str(),
                                          callback,
                                          (void *) data,
                                          &error_messages
    );

    if (database_identificator != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", error_messages);
        sqlite3_free(error_messages);
    } else {
        std::string choice;
        while (true) {
            std::cout << "Select menu: 1 is input, 2 is search, 3 is upgrade, 0 is exit" << std::endl << std::flush;
            std::cin >> choice;
            switch (hashit(choice)) {
                case input: {
                    std::string username, birthday, city;
                    long int phone_number(10);
                    while (true) {
                        std::cout << "\t"
                                  << "Enter data in sequence: phone number, username, birthday, city; If phone number equals -1, input sequence will be terminated"
                                  << std::endl << "\t\t";
                        std::cin >> phone_number;
                        if (phone_number == -1) {
                            std::cout << "\t" << "Input sequence terminated" << std::endl;
                            break;
                        }
                        std::cin >> username >> birthday >> city;
                        insert_function(database_pointer, (void *) data, error_messages, username, birthday, city,
                                        phone_number);
                    }
                    break;
                }
                case search: {
                    select_function(database_pointer, (void *) data, error_messages);
                    break;
                }
                case change: {
                    int id;
                    std::string chosen_field, change;
                    while (true) {
                        std::cout << "\t"
                                  << "Enter data in sequence:, id of the user, which field you intend to change(username/birthday/city/phonenumber), enter changes; If id equals -1, upgrade sequence will be terminated"
                                  << std::endl << "\t\t";
                        std::cin >> id;
                        if (id == -1) {
                            std::cout << "\t" << "Input sequence terminated" << std::endl;
                            break;
                        }
                        std::cin >> chosen_field >> change;
                        upgrade_function(database_pointer, (void *) data, error_messages, id, chosen_field, change);
                    }
                    break;
                }
                case sequence_exit: {
                    return 0;
                }
                default: {
                    std::cout << "Wrong input. Try again;" << std::endl;
                }
            }
        }
    }
    sqlite3_close(database_pointer);
    return 0;
}